import { IAuthorizationProvider, IAuthorizationRequest, IPermission, IPermissionRepository } from './type';

export class PermissionAuthorizationProvider<U> implements IAuthorizationProvider<U> {

  constructor(private permissionRepo: IPermissionRepository<U>) { }

  isSupported(request: IAuthorizationRequest<any>): boolean {
    return !!(request.permission);
  }

  async isAuthorized(request: IAuthorizationRequest<U>): Promise<boolean> {
    if (!request.user) {
      return false;
    }

    const permissions = await this.permissionRepo.getPermissions(request.user);

    if (!permissions || permissions.length === 0) {
      return false;
    }

    return permissions.some(permission => this.isMatch(permission, request.permission!));
  }

  private isMatch(permission: IPermission, requestingPermission: IPermission) {
    const { resource: permitResource, scope: permitScope } = permission;
    const { resource: requestingResource, scope: requestingScope } = requestingPermission;

    if (requestingResource === permitResource && requestingScope === permitScope) {
      return true;
    }

    if (requestingResource === permitResource && '*' === permitScope) {
      return true;
    }

    if ('*' === permitResource && requestingScope === permitScope) {
      return true;
    }

    if ('*' === permitResource && '*' === permitScope) {
      return true;
    }

    return false;
  }
}