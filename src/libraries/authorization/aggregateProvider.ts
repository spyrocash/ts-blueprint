import { IAuthorizationProvider, IAuthorizationRequest } from './type';

export class AggregateAuthorizationProvider<U> implements IAuthorizationProvider<U> {
  constructor(private providers: IAuthorizationProvider<U>[]) { }

  isSupported(): boolean {
    return true;
  }

  async isAuthorized(request: IAuthorizationRequest<U>): Promise<boolean> {
    // no provider provided -> permit
    if (!this.providers) return true;

    let supportedCounted = 0;

    for (const provider of this.providers) {
      if (provider.isSupported(request)) {
        supportedCounted++;

        const authorized = await provider.isAuthorized(request);

        if (authorized) {
          return true;
        }
      }
    }

    // no provider support the request -> permit
    if (supportedCounted === 0) {
      return true;
    }

    // falback -> deny
    return false;
  }

}