import { AmqpConnection } from '../connection';
import { MetaDataRegistry } from '../metaDataRegistry';
import { IPublicationConfig } from '../rascalConfig';

export function amqpPublish(queueName: string): Function
export function amqpPublish(pubConfig: IPublicationConfig): Function
export function amqpPublish(queueNameOrPubConfig: string | IPublicationConfig) {
  // tslint:disable-next-line: no-function-expression
  return function (_target: any, _propertyKey: string, descriptor: PropertyDescriptor) {
    if (typeof descriptor.value === 'function') {
      let pubConfig: IPublicationConfig = {};

      if (typeof queueNameOrPubConfig === 'string') {
        pubConfig.queue = queueNameOrPubConfig;
      } else {
        pubConfig = queueNameOrPubConfig;
      }

      MetaDataRegistry.REGISTER_PUBLISHER(pubConfig);

      descriptor.value = buildPublisher(pubConfig, descriptor.value);
    }
  }
}

function buildPublisher(pubConfig: IPublicationConfig, fn: Function) {
  return async function (this: any, ...args: any[]) {
    return AmqpConnection.PUBLISH(pubConfig, fn, args, this);
  }
}