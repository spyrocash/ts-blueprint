export async function invokePromiseOrFunction(fn: Function, context: any, args: any[]) {
  const result = fn.apply(context, args);

  if (result != null && typeof result === 'object' && typeof result.then === 'function') {
    return await result
  }

  return result;
}

export function safeJSONParse(value: any): {
  error: null | Error,
  value: any
} {
  try {
    const newValue = JSON.parse(value)
    return { error: null, value: newValue }
  } catch (error) {
    return { error, value }
  }
}