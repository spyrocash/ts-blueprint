
export interface RetryOptions {
  maxRetryTime?: number
  initialRetryTime?: number
  factor?: number
  multiplier?: number
  retries?: number
}

export const RETRY = "RETRY"
export const RETRY_THEN_DEAD_LETTER = "RETRY_THEN_DEAD_LETTER"

export interface RetryStrategy {
  type: typeof RETRY,
  retry: RetryOptions,
}

export interface RetryThenDeadLetterStrategy {
  type: typeof RETRY_THEN_DEAD_LETTER,
  retry: RetryOptions,
  deadLetterTopic: string
}

export type ErrorHandlingStrategy = RetryStrategy | RetryThenDeadLetterStrategy