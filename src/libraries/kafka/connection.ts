import Container from 'typedi';
import { defaultLogger } from '../../bootstrapLogger';
import { invokePromiseOrFunction } from '../kafka/util';
import { IKafkaConsumerConfig } from './decorators/kafkaConsumer';
import { IKafkaProducer } from './decorators/kafkaProducer';
import { IKafkaBroker } from './kafkaBroker';
import { MetaDataRegistry } from './metaDataRegistry';



export class KafkaConnection {
  static connection?: KafkaConnection

  constructor(private broker: IKafkaBroker) {
    KafkaConnection.connection = this;
  }

  static async PRODUCE(producerConfig: IKafkaProducer, originalFunction: Function, args: any[], context: any) {
    const result = await invokePromiseOrFunction(originalFunction, context, args)
    KafkaConnection.connection?.produce(producerConfig, result)
    return result
  }

  produce(produceConfig: IKafkaProducer, message: any) {
    return this.broker.produce(produceConfig, message)
  }

  async consumeAllConsumers() {
    for (const consumerMeta of MetaDataRegistry.consumerMetas) {
      const { consumerConfig, propertyKey, targetConstructor } = consumerMeta
      defaultLogger.info({
        event: "kafka_connection_consumer",
      }, `start consumer ${consumerConfig.subscribe.topic}`)

      const service = Container.get(targetConstructor) as any
      await this.subscribe(consumerConfig, service[propertyKey], service)
    }
  }

  subscribe(consumerConfig: IKafkaConsumerConfig, callBackFn: any, context: any) {
    return this.broker.consume(consumerConfig, callBackFn, context)
  }

}