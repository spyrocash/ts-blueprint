import { CompressionTypes, ProducerConfig } from 'kafkajs';
import { KafkaConnection } from '../connection';
import { MetaDataRegistry } from '../metaDataRegistry';


interface IConfig extends Omit<ProducerConfig, "transactionalId"> { }


interface IOptions {
  headers?: { [key: string]: any },
  acks?: number,
  timeout?: number,
  compression?: CompressionTypes,
}
export interface IKafkaProducer {
  topic: string,
  config?: IConfig,
  options?: IOptions
}

type KafkaProducerDecorator = (
  target: Object,
  propertyKey: string,
  descriptor: TypedPropertyDescriptor<(...args: any[]) => Promise<any>>) => void


export function kafkaProducer(topic: string): KafkaProducerDecorator
export function kafkaProducer(producerConfig: IKafkaProducer): KafkaProducerDecorator
export function kafkaProducer(topicOrProducerConfig: IKafkaProducer | string): KafkaProducerDecorator {
  return function (_target, _propertyKey, descriptor: PropertyDescriptor) {

    if (typeof descriptor.value === "function") {

      let produceConfig: any = {};

      if (typeof topicOrProducerConfig === 'string') {
        produceConfig.topic = topicOrProducerConfig
      } else {
        produceConfig = topicOrProducerConfig
      }

      MetaDataRegistry.REGISTER_PRODUCER(produceConfig)
      descriptor.value = builderProducer(produceConfig, descriptor.value);
    }
  }
}

function builderProducer(producerConfig: IKafkaProducer, originalFunction: Function) {
  return async function (this: any, ...args: any[]) {
    return KafkaConnection.PRODUCE(producerConfig, originalFunction, args, this)
  }
}
