import { ConsumerConfig, ConsumerSubscribeTopic } from 'kafkajs'
import { MetaDataRegistry } from '../metaDataRegistry'
import { ErrorHandlingStrategy, RETRY, RetryStrategy } from '../type'


interface IConsumerConfig extends Omit<ConsumerConfig, "groupId" | "retry"> {
}

export interface IKafkaConsumer {
  topic: string,
  groupId: string
  subscribe?: {
    fromBeginning: boolean,
  },
  options?: IConsumerConfig
  errorStrategy?: ErrorHandlingStrategy
}


export interface IKafkaConsumerConfig extends ConsumerConfig {
  subscribe: ConsumerSubscribeTopic
  errorStrategy?: ErrorHandlingStrategy
}


type KafkaConsumerDecorator = (
  target: Object,
  propertyKey: string,
  descriptor: TypedPropertyDescriptor<(...args: any[]) => Promise<any>>) => void




function buildKafkaConsumerConfig(consumerConfig: IKafkaConsumer): IKafkaConsumerConfig {


  let consumerConfigOverride!: IKafkaConsumerConfig
  const errorStrategyDefault: RetryStrategy = { type: RETRY, retry: { retries: 1 } }
  const { topic, options, subscribe, errorStrategy = errorStrategyDefault, ...config } = consumerConfig


  const fromBeginning = subscribe && subscribe.fromBeginning === true || false

  // TODO: set subscribe
  consumerConfigOverride = Object.assign({}, config, {
    subscribe: {
      fromBeginning,
      topic
    }
  })



  // TODO: override retry on top config of broker.consumer(...)
  consumerConfigOverride = Object.assign(
    {},
    consumerConfigOverride,
    { retry: errorStrategy.retry }
  )



  consumerConfigOverride = Object.assign({}, consumerConfigOverride, { ...options, errorStrategy })
  return consumerConfigOverride
}

export function kafkaConsumer(consumerConfig: IKafkaConsumer): KafkaConsumerDecorator {
  return function (target, propertyKey) {

    const consumerConfigOverride = buildKafkaConsumerConfig(consumerConfig)

    MetaDataRegistry.REGISTER_CONSUMER(
      consumerConfigOverride,
      target.constructor,
      propertyKey,
    )

  }
}