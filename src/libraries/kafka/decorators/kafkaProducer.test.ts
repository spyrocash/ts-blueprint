import { KafkaConnection } from '../connection';
import { ProduceMessage } from '../kafkaBroker';
import { kafkaProducer } from "./kafkaProducer";

jest.mock("../connection.ts")

describe('Kafka producer decorator', () => {

  //@ts-ignore
  class TestClass {
    @kafkaProducer("topic-test")
    async produce(params: string): Promise<ProduceMessage> {
      return {
        message: params
      }
    }

    @kafkaProducer({
      topic: "topic-test-2",
      config: {
        allowAutoTopicCreation: true
      },
      options: {
        timeout: 1000
      }
    })
    async produceWithConfig(params: string): Promise<ProduceMessage> {
      return {
        message: params
      }
    }
  }

  it('should call produce method instead of original', async () => {

    const mockProduce = KafkaConnection.PRODUCE as jest.Mock;
    mockProduce.mockReturnValue("mocked result")

    const test = new TestClass();
    const result = await test.produce("m1");

    expect(mockProduce.mock.calls[0][0]).toEqual({
      topic: 'topic-test',
    })
    expect(typeof mockProduce.mock.calls[0][1]).toBe("function")
    expect(mockProduce.mock.calls[0][2]).toEqual(["m1"])
    expect(mockProduce.mock.calls[0][3]).toBe(test)
    expect(result).toBe("mocked result")

  });

  it("should build producer config successfully", async () => {
    const mockProduce = KafkaConnection.PRODUCE as jest.Mock;
    mockProduce.mockReturnValue("mocked result")
    const test = new TestClass();
    const result = await test.produceWithConfig("m1");

    expect(mockProduce.mock.calls[1][0]).toEqual({
      topic: 'topic-test-2',
      config: {
        allowAutoTopicCreation: true,
      },
      options: {
        timeout: 1000
      }
    })

    expect(mockProduce.mock.calls[1][2]).toEqual(["m1"])
    expect(mockProduce.mock.calls[1][3]).toBe(test)
    expect(result).toBe("mocked result")

  })

  it("should throw error raised by original function", async () => {
    const mockProduce = KafkaConnection.PRODUCE as jest.Mock;
    mockProduce.mockRejectedValue(new Error("test error"))

    const test = new TestClass();
    const check = () => test.produce("m1");
    expect(check()).rejects.toThrow(new Error("test error"))

  })



});