import { mock, mockReset } from 'jest-mock-extended'
import * as mongoose from 'mongoose'
import { ILogger } from '../logger/logger.interface'
import { MongoConnection } from './mongoConnection'

jest.mock('mongoose')

describe('mongoConnection.test', () => {
  const mockConnect = mongoose.connect as jest.Mock
  const mockLogger = mock<ILogger>()

  beforeEach(() => {
    mockReset(mockLogger)
    mockReset(mockConnect)
  })

  it('should connect asynchronouselly normally', async () => {
    mockConnect.mockResolvedValueOnce(true)

    const mongoConnection = new MongoConnection({
        uri: 'example.com/test',
        options: {
          dbName: 'test'
        }
      },
      mockLogger
    )

    await mongoConnection.connect()

    expect(mockConnect).toHaveBeenCalled()
    expect(mockConnect).toHaveBeenCalledWith(
      'example.com/test', {
        dbName: 'test'
      }
    )
    expect(mockLogger.info).toHaveBeenCalledTimes(1)

    await mongoConnection.connect()

    expect(mockConnect).toHaveBeenCalledTimes(1)
    expect(mockLogger.info).toHaveBeenCalledTimes(1)
  })

  it('should throw error when connect function throw error async', async () => {
    mockConnect.mockRejectedValueOnce(new Error('connection error'))

    const mongoConnection = new MongoConnection({
        uri: 'example.com/test',
        options: {
          dbName: 'test'
        }
      },
      mockLogger
    )

    await expect(mongoConnection.connect()).rejects.toThrow('connection error')

    expect(mockConnect).toHaveBeenCalled()
    expect(mockLogger.info).not.toHaveBeenCalled()
  })
})
