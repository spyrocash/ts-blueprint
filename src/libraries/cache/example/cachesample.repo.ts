import { cache } from '../decorators/cache';
import { cacheKey } from '../decorators/cacheKey';
import { evictCache } from '../decorators/evictCache';

export interface ISample {
  id: string
  text: string
}

export class CacheSampleRepo {
  @cache() // key is 'getSampleById1-${id}'
  async getSampleById1(@cacheKey() id: string, text: string): Promise<ISample | undefined> {
    return { id, text };
  }

  @evictCache('getSampleById1') // key is 'getSampleById1-${sample.id}'
  async updateSample1(@cacheKey('id') sample: ISample): Promise<ISample> {
    return sample;
  }

  @cache({ key: 'sample2', ttl: 300 }) // key is 'sample2-${id}'
  async getSampleById2(@cacheKey() id: string, text: string): Promise<ISample | undefined> {
    return { id, text };
  }

  @evictCache('sample2') // key is 'sample2-${id}'
  async updateSample2(@cacheKey() id: string, text: string): Promise<ISample> {
    return { id, text };
  }

  @cache({ key: (sample) => `sample3-${sample.id}${sample.text}` }) // key is 'sample3-${sample.id}${sample.text}'
  async getSampleById3(sample: ISample): Promise<ISample | undefined> {
    return sample;
  }

  @evictCache((sample) => `sample3-${sample.id}${sample.text}`) // key is 'sample3-${sample.id}${sample.text}'
  async updateSample3(sample: ISample): Promise<ISample> {
    return sample;
  }

  @cache() // key is listSample
  async listSample(): Promise<ISample[]> {
    return [];
  }

  @evictCache('listSample')
  async evictListSample(): Promise<ISample[]> {
    return [];
  }

}