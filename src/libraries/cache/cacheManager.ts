import { ILogger } from '../logger/logger.interface';
import { ICacheEvictMeta, ICacheKeyMeta, ICacheMeta } from './decorators/meta';
import { CacheKey, ICacheProvider } from './interface';

export class CacheManager {
  private static cacheManager: CacheManager;

  private constructor(private logger: ILogger, private cacheProvider: ICacheProvider) { }

  static NEW_INSTANCE(logger: ILogger, cacheProvider: ICacheProvider) {
    CacheManager.cacheManager = new CacheManager(logger, cacheProvider);
    return CacheManager.cacheManager;
  }

  wrap(context: any, originalFunction: Function, cacheMeta: ICacheMeta, args: any[]) {
    const { cacheOption, cacheKeyMetas } = cacheMeta;
    const { key, ...cacheConfig } = cacheOption;

    const cacheKey = this.getCacheKey(args, cacheOption.key, cacheKeyMetas);

    return this.cacheProvider.wrap(cacheKey, () => {
      this.logger.debug('cache missed with key: ', cacheKey);
      return originalFunction.apply(context, args);
    }, cacheConfig);
  }

  evict(context: any, originalFunction: Function, cacheEvictMeta: ICacheEvictMeta, args: any[]) {
    const { cacheEvictOption, cacheKeyMetas } = cacheEvictMeta;

    const cacheKey = this.getCacheKey(args, cacheEvictOption.key, cacheKeyMetas);

    const result = originalFunction.apply(context, args);

    // no error -> proceed cache del
    if (typeof result === 'object' && 'then' in result) {
      return result.then((original: any) => {
        this.deleteCache(cacheKey);
        return original;
      });
    } else {
      this.deleteCache(cacheKey)
    }

    return result;
  }

  private deleteCache(cacheKey: string) {
    this.cacheProvider.del(cacheKey, (err) => {
      if (err) {
        this.logger.error(err, 'error deleting cache: ', cacheKey);
      } else {
        this.logger.debug('cache deleted with key: ', cacheKey);
      }
    });
  }

  private getCacheKey(args: any[], key: CacheKey<any>, cacheKeyMetas: ICacheKeyMeta[]) {
    const mainKey = key;

    if (typeof mainKey === 'string') {
      if (args.length === 0) return mainKey; // single main key only

      if (cacheKeyMetas.length === 0) {
        const subKey = args.join(''); // concat all arguments as sub key
        return `${mainKey}-${subKey}`;
      }

      // concat subkey from key metas
      const subKey = cacheKeyMetas.map(km => {
        const argValue = args[km.parameterIndex];

        if (argValue && km.propertyKey) {
          return argValue[km.propertyKey];
        }
        return argValue;
      }).join('');
      return `${mainKey}-${subKey}`;
    }

    return mainKey(...args);
  }

  static WRAP_CACHE(originalFunction: Function, cacheMeta: ICacheMeta) {
    // tslint:disable-next-line: no-function-expression
    return function (this: any, ...args: any[]) {
      return CacheManager.cacheManager.wrap(this, originalFunction, cacheMeta, args);
    }
  }

  static WRAP_CACHE_EVICT(originalFunction: Function, cacheEvictMeta: ICacheEvictMeta) {
    // tslint:disable-next-line: no-function-expression
    return function (this: any, ...args: any[]) {
      return CacheManager.cacheManager.evict(this, originalFunction, cacheEvictMeta, args);
    }
  }

}