import { anyFunction, mock } from 'jest-mock-extended';
import { ILogger } from '../logger/logger.interface';
import { CacheManager } from "./cacheManager";
import { ICacheEvictMeta, ICacheMeta } from './decorators/meta';
import { ICacheProvider } from './interface';

describe('cacheManager.test', () => {

  describe('Cache Value', () => {

    it('should return cache value when cache available', async () => {
      const meta: ICacheMeta = {
        cacheOption: {
          key: 'testKey',
          ttl: 5,
          max: 5
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, realFn, mockCacheProvider } = buildCacheTestCase(meta, 'actual value', 'cached value');

      const result = await wrappedFn();

      expect(result).toBe('cached value');
      expect(realFn).not.toBeCalled();
      expect(mockCacheProvider.wrap).toBeCalledWith('testKey', anyFunction(), { ttl: 5, max: 5 });
    })

    it('should return cache value when cache is not available', async () => {
      const meta: ICacheMeta = {
        cacheOption: {
          key: 'testKey',
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, realFn, mockCacheProvider } = buildCacheTestCase(meta, 'actual value');

      const result = await wrappedFn();

      expect(result).toBe('actual value');
      expect(realFn).toBeCalled();
      expect(mockCacheProvider.wrap).toBeCalledWith('testKey', anyFunction(), {});
    })

  })

  describe('Cache Key', () => {

    it('should return main cache key', async () => {
      const meta: ICacheMeta = {
        cacheOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, mockCacheProvider } = buildCacheTestCase(meta, 'actual value', 'cached value');

      await wrappedFn();

      expect(mockCacheProvider.wrap).toBeCalledWith('mainKey', anyFunction(), {});
    })

    it('should return mainKey with args as subKey', async () => {
      const meta: ICacheMeta = {
        cacheOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, mockCacheProvider } = buildCacheTestCase(meta, 'actual value', 'cached value');

      await wrappedFn('1');

      expect(mockCacheProvider.wrap).toBeCalledWith('mainKey-1', anyFunction(), {});
    })

    it('should return mainKey with primitive key meta as subKey', async () => {
      const meta: ICacheMeta = {
        cacheOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: [
          { parameterIndex: 1 }
        ]
      };

      const { wrappedFn, mockCacheProvider } = buildCacheTestCase(meta, 'actual value', 'cached value');

      await wrappedFn('a', '1');

      expect(mockCacheProvider.wrap).toBeCalledWith('mainKey-1', anyFunction(), {});
    })

    it('should return mainKey with object key meta as subKey', async () => {
      const meta: ICacheMeta = {
        cacheOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: [
          { parameterIndex: 0, propertyKey: 'id' }
        ]
      };

      const { wrappedFn, mockCacheProvider } = buildCacheTestCase(meta, 'actual value', 'cached value');

      await wrappedFn({ id: '1' });

      expect(mockCacheProvider.wrap).toBeCalledWith('mainKey-1', anyFunction(), {});
    })

    it('should return key with key function and ignore key meta', async () => {
      const meta: ICacheMeta = {
        cacheOption: {
          key: (obj) => `mainKey-${obj.id}`
        },
        cacheKeyMetas: [
          { parameterIndex: 1 }
        ]
      };

      const { wrappedFn, mockCacheProvider } = buildCacheTestCase(meta, 'actual value', 'cached value');

      await wrappedFn({ id: '1' }, 'a');

      expect(mockCacheProvider.wrap).toHaveBeenCalledWith('mainKey-1', anyFunction(), {});
    })

  })

  describe('Cache Eviction', () => {

    it('should delete cache when function called success', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, realFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      const result = await wrappedFn();

      expect(result).toBe('actual value');
      expect(realFn).toBeCalled();
      expect(mockCacheProvider.del).toBeCalledWith('mainKey', anyFunction());
    })

    it('should delete cache when function called return resolved promise', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, realFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      realFn.mockResolvedValue('actual value');

      const result = await wrappedFn();

      expect(result).toBe('actual value');
      expect(realFn).toBeCalled();
      expect(mockCacheProvider.del).toBeCalledWith('mainKey', anyFunction());
    })

    it('should not delete cache when function called error', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, realFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      realFn.mockImplementation(() => {
        throw new Error('test error');
      });

      expect(wrappedFn).toThrow('test error');
      expect(mockCacheProvider.del).not.toBeCalled();
    })

    it('should not delete cache when function called return rejected promise', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, realFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      realFn.mockRejectedValue(new Error('promise error'));

      await expect(wrappedFn()).rejects.toThrow('promise error');
      expect(mockCacheProvider.del).not.toBeCalled();
    })

    it('should log error when cache delete error', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, mockCacheProvider, mockLogger } = buildCacheEvictTestCase(meta, 'actual value');

      mockCacheProvider.del.mockImplementation((_key, cb) => {
        cb(new Error('test error'));
      });

      const result = await wrappedFn();

      expect(result).toBe('actual value');
      expect(mockLogger.error).toBeCalled();
    })

  })

  describe('Cache Eviction Key', () => {

    it('should return main cache key', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      await wrappedFn();

      expect(mockCacheProvider.del).toBeCalledWith('mainKey', anyFunction());
    })

    it('should return mainKey with args as subKey', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: []
      };

      const { wrappedFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      await wrappedFn('1');

      expect(mockCacheProvider.del).toBeCalledWith('mainKey-1', anyFunction());
    })

    it('should return mainKey with primitive key meta as subKey', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: [
          { parameterIndex: 1 }
        ]
      };

      const { wrappedFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      await wrappedFn('0', '1');

      expect(mockCacheProvider.del).toBeCalledWith('mainKey-1', anyFunction());
    })

    it('should return mainKey with object key meta as subKey', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: 'mainKey'
        },
        cacheKeyMetas: [
          { parameterIndex: 0, propertyKey: 'id' }
        ]
      };

      const { wrappedFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      await wrappedFn({ id: '1' });

      expect(mockCacheProvider.del).toBeCalledWith('mainKey-1', anyFunction());
    })

    it('should return key with key function and ignore key meta', async () => {
      const meta: ICacheEvictMeta = {
        cacheEvictOption: {
          key: (obj) => `mainKey-${obj.id}`
        },
        cacheKeyMetas: [
          { parameterIndex: 1, propertyKey: 'id' }
        ]
      };

      const { wrappedFn, mockCacheProvider } = buildCacheEvictTestCase(meta, 'actual value');

      await wrappedFn({ id: '1' });

      expect(mockCacheProvider.del).toBeCalledWith('mainKey-1', anyFunction());
    })

  })

})

function buildCacheTestCase(meta: ICacheMeta, actualValue: any, cacheValue?: any) {
  const mockCacheProvider = mock<ICacheProvider>();
  const mockLogger = mock<ILogger>();

  if (cacheValue) {
    mockCacheProvider.wrap.mockResolvedValue('cached value');
  } else {
    mockCacheProvider.wrap.mockImplementation((_key, cb) => {
      return Promise.resolve(cb());
    });
  }

  CacheManager.NEW_INSTANCE(mockLogger, mockCacheProvider);

  const realFn = jest.fn();
  realFn.mockReturnValue(actualValue);

  const wrappedFn = CacheManager.WRAP_CACHE(realFn, meta);

  return { realFn, wrappedFn, mockCacheProvider };
}

function buildCacheEvictTestCase(meta: ICacheEvictMeta, actualValue: any) {
  const mockCacheProvider = mock<ICacheProvider>();
  const mockLogger = mock<ILogger>();

  CacheManager.NEW_INSTANCE(mockLogger, mockCacheProvider);

  const realFn = jest.fn();
  realFn.mockReturnValue(actualValue);

  const wrappedFn = CacheManager.WRAP_CACHE_EVICT(realFn, meta);

  return { realFn, wrappedFn, mockCacheProvider, mockLogger };
}