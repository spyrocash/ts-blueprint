import { ILocalTracer } from 'hpropagate/lib/tracer';
import { Tracer } from 'opentracing';

export interface ILocalTracingInfo {
  localTracer: ILocalTracer
  localTracingKey?: string
}

export interface ITracingInfo extends ILocalTracingInfo {
  headersToPropagate: string[]
  globalTracer: Tracer
  globalTracingKey?: string
}