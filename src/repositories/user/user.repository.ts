import { IAnonoymousableUser } from '../../libraries/authentication/type';

export interface IUser extends IAnonoymousableUser {
  _id: string
  firstname: string
  lastname: string
  roles?: string[]
}