import { Body, Controller, Post } from 'routing-controllers';
import { KafkaDemoService } from '../../../adapters/kafka/demo.kafka';


@Controller('/kafka')
export class KafkaController {

  constructor(private kafkaDemoService: KafkaDemoService) {
  }

  @Post('/simple/error-dead-letter')
  async sendProducerSimpleDeadLetter(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimpleErrorDeadLetter(body)
    return 'ok'
  }

  @Post('/simple/error')
  async sendProducerSimpleError(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimpleError(body)
    return 'ok'
  }

  @Post('/simple')
  async sendPublish(@Body() body: any) {
    this.kafkaDemoService.sendProducerSimple(body)
    return 'ok'
  }

}
