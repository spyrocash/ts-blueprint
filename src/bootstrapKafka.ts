import { SASLMechanism } from 'kafkajs';
import './adapters/kafka/index';
import { config } from "./bootstrapConfig";
import { defaultLogger } from './bootstrapLogger';
import { createKafkaConnection, IKafkaConnectionOptions } from './libraries/kafka';
import { ensureConfigKeys } from "./utils/configUtil";




async function bootstrapKafka() {

  const logger = defaultLogger;
  try {

    if (config.kafka.KAFKA_SASL === true) {
      ensureConfigKeys(config.kafka,
        'KAFKA_BROKERS',
        'KAFKA_MECHANISM',
        'KAFKA_USERNAME',
        'KAFKA_PASSWORD'
      )
    } else {
      ensureConfigKeys(config.kafka, 'KAFKA_BROKERS')
    }


    const connectionUrl = config.kafka.KAFKA_BROKERS?.split(",") || []

    let options: IKafkaConnectionOptions = {
      brokers: connectionUrl,
      topic_prefix: config.kafka.KAFKA_TOPIC_PREFIX || ''

    }

    if (config.kafka.KAFKA_MECHANISM && config.kafka.KAFKA_USERNAME && config.kafka.KAFKA_PASSWORD) {
      options.sasl = {
        mechanism: config.kafka.KAFKA_MECHANISM as SASLMechanism,
        username: config.kafka.KAFKA_USERNAME,
        password: config.kafka.KAFKA_PASSWORD
      }
    }

    if (config.kafka.KAFKA_SSL === true) {
      options.ssl = true
    }


    await createKafkaConnection(options, logger)
    logger.info({ event: 'bootstrap_kafka' }, `Successfully connect to kafka server`);

  } catch (error) {
    logger.error(error, { event: 'bootstrap_kafka' }, `Error connected to kafka server`)
    process.exit(-1)
  }

}

bootstrapKafka();